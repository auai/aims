import React, { useEffect, useRef, useState } from "react";
import * as cornerstone from "cornerstone-core";
import * as cornerstoneMath from "cornerstone-math";
import * as cornerstoneTools from "cornerstone-tools";
import Hammer from "hammerjs";
import * as cornerstoneWADOImageLoader from "cornerstone-wado-image-loader";
import * as dicomParser from "dicom-parser";

cornerstoneTools.external.cornerstone = cornerstone;
cornerstoneTools.external.cornerstoneMath = cornerstoneMath;
cornerstoneTools.external.Hammer = Hammer;
cornerstoneTools.init({
	touchEnabled: true,
	globalToolSyncEnabled: true,
	showSVGCursors: false,
	autoResizeViewports: true
});

cornerstoneWADOImageLoader.external.cornerstone = cornerstone;
cornerstoneWADOImageLoader.external.dicomParser = dicomParser;
cornerstoneWADOImageLoader.configure({ useWebWorkers: false });


const PanTool = cornerstoneTools.PanTool;
const ZoomMouseWheelTool = cornerstoneTools.ZoomMouseWheelTool;
const ZoomTouchPinchTool = cornerstoneTools.ZoomTouchPinchTool;

cornerstoneTools.addTool(PanTool);
cornerstoneTools.addTool(ZoomMouseWheelTool,{
	configuration: {
		minScale: 0.1,
		maxScale: 20.0,
	}
});
cornerstoneTools.addTool(ZoomTouchPinchTool)

const CornerstoneElement = ({file}) => {
	const viewportRef = useRef(null);
	// const [image, setImage] = useState(undefined);

	useEffect(()=>{
		const { current: element } = viewportRef
		
		//enable dom elem to use cornerstone
		cornerstone.enable(element)

		//load file
		if(file){
			document.querySelector('.dicom-viewer').style.zIndex = '2'
			console.log('index cse: file>',file)
			const imageID = cornerstoneWADOImageLoader.wadouri.fileManager.add(file)
			console.log('index cse: file2>',imageID)
			cornerstone.loadImage(imageID).then((image)=>{
				cornerstone.displayImage(element, image);
				cornerstoneTools.setToolActive("Pan", { mouseButtonMask: 1 });
				cornerstoneTools.setToolActive('ZoomMouseWheel', { mouseButtonMask: 1 })
				cornerstoneTools.setToolActive('ZoomTouchPinch', { mouseButtonMask: 1 })
			})
		}
	},[file])
	const testScroll = () =>{
		console.log('scrolling!')
	}
	
	return (
		<div className="viewportElement dicom-viewer" ref={viewportRef} onScroll={testScroll}>
			<canvas className="cornerstone-canvas"/>
		</div>
	);
}

export default CornerstoneElement;