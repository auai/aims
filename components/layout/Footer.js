import * as React from 'react'
import {
	Typography,
	Grid,
	Paper,
	Button,
	Container
} from '@material-ui/core'

import Link from 'next/link';
import TwitterIcon from '@material-ui/icons/Twitter';
import LinkedInIcon from '@material-ui/icons/LinkedIn';

const Footer = () => {
	return (
		<Grid container className="page-footer">
			<Grid item xs={12}>
				<Typography className="page-footer-logo" variant="h4" gutterBottom>
					AIMS
				</Typography>
				<Typography className="page-footer-company-description" variant="subtitle1" gutterBottom>
					Alixir's AIMS is a smart AI Mammogram viewer and Breast cancer detection tool that lets you view your digital mammogram on web. It enables users to get second opinion for their digital mammogram, visualize the location of tumor upon detection and save images in all format.
				</Typography>
				{/* <div className="page-footer-links">
					<Link href='/'>FAQs</Link>
					<Link href='/'>Pricing</Link>
				</div> */}
				<Typography className="page-footer-social-links" variant="subtitle1" gutterBottom>
					Find Us On
					<br />
					<br />
					<Link href="https://twitter.com/alixirhq">
						<TwitterIcon className="social-link"/>
					</Link>
					<Link href="https://www.linkedin.com/company/alixir/">
						<LinkedInIcon className="social-link"/>
					</Link>
				</Typography>
			</Grid>
		</Grid>
	)
}

export default Footer;