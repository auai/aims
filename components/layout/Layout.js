import * as React from 'react';
import {
    Grid,
    Typography,
    Container,
    Paper
} from '@material-ui/core'

import Header from './Header'
import Footer from './Footer'

const Layout = props =>{
    return(
        <React.Fragment>
            <Typography component="div">
                <Grid container>
                    <Grid item xs={12}>
                        <Header/>
                        <Paper className="page-content">
                            <Container fixed maxWidth="xl">
                                {props.children}
                            </Container>
                        </Paper>
                        <Footer/>
                    </Grid>
                </Grid>
            </Typography>
        </React.Fragment>
    )
}

export default Layout