import * as React from 'react';
import {
    AppBar,
	Toolbar,
	Typography,
	Button,
	// IconButton
} from '@material-ui/core'

// import MenuIcon from '@material-ui/icons/Menu';

const Header = () =>{
	return (
		<AppBar position="static" className="page-header">
			<Toolbar>
				{/* <IconButton edge="start"  className="menuButton" color="inherit" aria-label="menu">
					<MenuIcon />
				</IconButton> */}
				<Typography variant="h6" className="title">
					AIMS(LOGO)
				</Typography>
				<div className="header-buttons">
					<Button color="inherit">Login</Button>
				</div>
			</Toolbar>
		</AppBar>
	)
}

export default Header;