import React from 'react'

export const Context = React.createContext()

export function useGlobalState() {
	return React.useContext(Context)
}

export class GlobalStateProvider extends React.Component {
	constructor() {
		super()
		this.state = {
			user: null,
			token: null,
			authHeader: { Authorization: `Bearer <null>` },
		}
	}

	set(key, val) {
		this.setState({ [key]: val })
	}

	async componentDidMount(){
		// fetch auth status from Backend
		// get token
	}

	render(){
		return(
			<Context.Provider
				value={Object.assign(this.state, {
					set: (key, value) => this.set(key, value),
				})}
			>
				{this.props.children}
			</Context.Provider>
		)
	}

}
  