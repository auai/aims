import Head from 'next/head'
import { GlobalStateProvider} from '../support/useGlobalState'
import Layout from '../components/layout/Layout'

import '../styles/main.scss'

const MyApp = ({Component, pageProps}) => (
    <GlobalStateProvider>
        <Head>
			<title>ALIXIR - AIMS</title>
			<link rel="icon" href="/logo.png" />
            <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
		</Head> 
        <div className="page">
            <Layout>
                <Component {...pageProps} />
            </Layout>
        </div>
    </GlobalStateProvider>
)

export default MyApp