import React, { useState } from 'react'

import Dropzone from 'react-dropzone'
import dynamic from 'next/dynamic'
import {
	Grid,
	Typography,
	Button
} from '@material-ui/core'

const CornerstoneElement = dynamic(
  () => import('../../components/CornerstoneElement'),
  { ssr: false }
)

export default function Home() {
	const [activeFile, setActiveFile] = useState(null)

	const fileHandler = (files) =>{
		if(files || files[0]){
			console.log('file upload>',files)
			setActiveFile(files[0])
		}
	}
	
	return (
		<Grid container spacing={3} className="app-container">
			<Grid item xs={8} className="dicom-container-grid">
				<div className='dicom-container'>
					<Dropzone onDrop={files => fileHandler(files)} multiple={false}>
						{({getRootProps, getInputProps}) => (
							<section className="dropzone-container">
								<div {...getRootProps()}>
									<input {...getInputProps()} />
									<p>Drag 'n' drop some files here, or click to select files</p>
								</div>
							</section>
						)}
					</Dropzone>
					<CornerstoneElement file={activeFile}/>
				</div>
			</Grid>
			<Grid item xs={4} className="app-toolnav">
				<Button className="button-primary">
					<span>Run AIMS</span>
				</Button>
			</Grid>
		</Grid>
	)
}
