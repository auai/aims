import * as React from 'react';
import Link from 'next/link';

import {
	Grid,
	Typography,
	Button,
	Icon
} from '@material-ui/core'

import AddCircleIcon from '@material-ui/icons/AddCircle';

const Home = () => {
	return(
		<Grid container spacing={2} className="home">
			<Grid item xs={12} className="banner">
				<Grid container justify="center" spacing={5}>
					<Grid md={6} sm={12} className="banner-text">
						<Typography className="banner-text-title" variant="h2" component="h2" gutterBottom>
							Get Second opinion
						</Typography>
						
						<Typography className="banner-text-subtitle" variant="h4" component="h2" gutterBottom>
							Get Second opinion
						</Typography>
						<Link href ="/app" shallow>
							<Button className="button-primary">
								<AddCircleIcon/>
								<span>Try Now</span>
							</Button>
						</Link>
					</Grid>
					<Grid md={6} sm={12} className="banner-animation">
						Some Kind of image transition
					</Grid>
				</Grid>
			</Grid>
		</Grid>
	)
}

export default Home;